FROM ubuntu
ARG BUILD_DATE
ARG VERSION
ARG VCS_REF
LABEL org.label-schema.schema-version="1.0" \
      org.label-schema.name="firebase-tools" \
      org.label-schema.version=${VERSION} \
      org.label-schema.build-date=${BUILD_DATE} \
      org.label-schema.description="Firebase CLI on Ubuntu with nodejs, java and curl" \
      org.label-schema.url="https://github.com/firebase/firebase-tools/" \
      org.label-schema.vcs-url="https://gitlab.com/memogarrido/firebase-tools" \
      org.label-schema.vcs-ref=${VCS_REF}
ENV FIREBASE_TOOLS_VERSION=${VERSION}
ENV HOME=/home/node
EXPOSE 4000
EXPOSE 5000
EXPOSE 5001
EXPOSE 8080
EXPOSE 8085
EXPOSE 9000
EXPOSE 9005
EXPOSE 9099
EXPOSE 9199
RUN apt update && apt install openjdk-11-jdk -y && apt install curl -y && \
    curl -sL https://deb.nodesource.com/setup_lts.x | bash -  && \
    apt install unzip -y && apt install nodejs -y  && \
    npm install -g firebase-tools && \
    apt-get install apt-transport-https ca-certificates gnupg -y && \
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg |  tee /usr/share/keyrings/cloud.google.gpg && \
    apt-get update && apt-get install google-cloud-cli -y && \
    firebase setup:emulators:database && \
    firebase setup:emulators:firestore && \
    firebase setup:emulators:pubsub && \
    firebase setup:emulators:storage && \
    firebase -V && \
    java -version && \
    gsutil version && \
    node -v
USER root
VOLUME $HOME/.cache
WORKDIR $HOME
CMD ["sh"]